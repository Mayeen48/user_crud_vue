<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('register', 'AuthController@register');

});
Route::group([
    'middleware' => 'jwt.auth',
], function ($router) {
    Route::apiResources(
        [
            'users' => 'AuthController'
        ]
    );
});
