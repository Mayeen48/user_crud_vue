<?php

namespace App\Http\Controllers;
use App\Repository\UserRepository;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\Models\ADMIN\User;
use Validator;


class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    protected $userRepo;
    public function __construct(UserRepository $userRepository) {
        $this->userRepo=$userRepository;
        $this->middleware('auth:api', ['except' => ['login', 'register']]);

    }

    public function index(){
        $users=$this->userRepo->getAll();
        return response()->json(['users'=>$users]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        // return $request->all();
        return $this->userRepo->login($request);

    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        return $this->userRepo->register_user($request);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */


}
