<p align="left">User Crud Vue</p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About CRUD

# php/composer already installed

# git clone
git clone https://gitlab.com/Mayeen48/user_crud_vue.git

composer install

# make .env
cp .env.example .env
# edit .env
Change you DB info
# database info

composer update

php artisan key:generate

php artisan migrate --seed

npm install
php artisan jwt:secret

Add MIX_APP_URL="${APP_URL}" and JWT_TTL=1  on your .env file

npm run dev
OR
npm run watch


#Note: Ensure that Node.js and npm are installed on your computer.

### About Task done
1. Laravel 6
2. API Impliment
3. API Authentication with JWT
4. CSRF token verified
5. Repository pattern applied
6. beforeCreate hook auth check
7. Vue filter added

### About Test
User: super_admin@gmail.com
Password: Qe75ymSr
