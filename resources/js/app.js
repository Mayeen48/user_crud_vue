/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
import VueRouter from 'vue-router';
import { routes } from '../js/routes';
import axios from 'axios'
import myMixin from '../js/myMixin'
import VueAxios from 'vue-axios'
import Swal from 'sweetalert2'
Vue.use(VueRouter)
window.Fire = new Vue();

Vue.use(VueAxios, axios)
    // axios.defaults.baseURL = `${process.env.MIX_APP_URL}api`
    // console.log(axios.defaults.baseURL)
axios.create({
    baseURL: `${process.env.APP_URL}`
});


var vm = new Vue({
    mixins: [myMixin]
})
Vue.mixin(myMixin)

Vue.use(require('vue-moment'));
window.Swal = Swal
    /**
     * The following block of code may be used to automatically register your
     * Vue components. It will recursively scan this directory for the Vue
     * components and automatically register them with their "basename".
     *
     * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
     */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
// axios.interceptors.response.use(function(response) {
//     // Any status code that lie within the range of 2xx cause this function to trigger
//     // Do something with response data
//     console.log(response)
//     if (response.data.status == 401) {
//         return router.push({ name: 'login' });
//     }
//     return response;
// }, function(error) {
//     // return router.push({ name: 'login' });
//     // Any status codes that falls outside the range of 2xx cause this function to trigger
//     // Do something with response error
//     return Promise.reject(error);
// });
var router = new VueRouter({
    routes: routes,
    linkActiveClass: "active", // active class for non-exact links.
    linkExactActiveClass: "active", // active class for *exact* links.
    mode: 'history',
    base: '/user_crud_vue'
});
router.beforeEach((to, from, next) => {
    var localStorageToken = vm.getWithExpiry('token')
    axios.defaults.headers.common['Authorization'] = `Bearer ${localStorageToken}`;
    var loggedIn = (localStorageToken == null ? false : true);

    if (loggedIn) {
        if (to.name === 'login') {
            return next({ name: 'users' })
        }
    } else {
        if (to.name === 'users') {
            return next({ name: 'login' })
        }
    }
    next();
})

import App from '../js/components/app.vue'
new Vue({
    router: router,
    render: h => h(App)
}).$mount("#app");