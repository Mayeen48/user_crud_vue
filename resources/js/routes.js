import Login from '../js/components/login.vue'
import register from '../js/components/register.vue'
import dashboard from '../js/components/dashboard.vue'

export const routes = [{
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'register',
        component: register
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: dashboard
    },

]
